cd api/
npm i
sudo docker-compose up
npm run db:create
npm run db:migrate
npm run db:seed-dev
npm run start

cd client/
npm i
npm run start

http://localhost:4200
