config/config.development.ts
database: 'bench',
port: 3306,
username: 'root',
password: '123',

создать базу
npm run db:create

mkdir sequelize
cd sequelize/
https://sequelize.org/master/manual/migrations.html
npx sequelize-cli init

в файле sequelize/config/config.json
"development": {
    "username": "root",
    "password": "123",
    "database": "bench",
    "host": "127.0.0.1",
    "dialect": "mysql",
    "operatorsAliases": 0
  },

сгенерировать файл миграции
npx sequelize-cli model:generate --name Projects --attributes title:string,description:string
npx sequelize-cli model:generate --name user --attributes email:string,password:string,firstName:string,lastName:string
cp migrations/(data+time)-create-projects.js ../db/migrations/

npm run db:migrate
last time I use this:
npx sequelize-cli db:migrate

npx sequelize-cli seed:generate --name demo-project
edit: sequelize/seeders/(date+time)-demo-projects.js
'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.bulkInsert('Projects', [{
        title: 'Vue.js + Koa',
        description: 'Vue is a progressive framework for building user interfaces.'
      }], {});
  },
  down: (queryInterface, Sequelize) => {
      return queryInterface.bulkDelete('Projects', null, {});
  }
};

cp seeders/(date+time)-demo-projects.js ../db/seeders-dev/

npm run db:seed-dev

add functionality:
nest g mo projects
nest g co projects
nest g s projects
nest g pr projects

npx sequelize-cli model:generate --name Projects --attributes email:string,password:string,firstName:string,lastName:string
