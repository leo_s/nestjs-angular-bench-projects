import { Dialect } from 'sequelize/types';

export const config = {
    database: {
        dialect: 'mysql' as Dialect,
        host: 'localhost',
        port: 3306,
        username: 'root',
        password: '123',
        database: 'bench',
        logging: false,
    },
    jwtPrivateKey: 'jwtPrivateKey',
    signOptions: { expiresIn: '7 days' }, //Eg: 48, "2 days", "10h", "7d". (48 - time in seconds)
};
