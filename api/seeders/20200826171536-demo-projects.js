'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('project', [
      {
        title: 'Vue',
        description: 'Vue is a progressive framework for building user interfaces.'
      },
      {
        title: 'Koa',
        description: 'Koa is a new web framework designed by the team behind Express, which aims to be a smaller, more expressive, and more robust foundation for web applications and APIs.'
      },
      {
        title: 'Express',
        description: 'Express is a minimal and flexible Node.js web application framework that provides a robust set of features for web and mobile applications.'
      },
      {
        title: 'React',
        description: 'React makes it painless to create interactive UIs.'
      },
    ], {});
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('project', null, {});
  }
};
