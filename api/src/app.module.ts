import { Module } from '@nestjs/common';
import { SharedModule } from './shared/shared.module';
import { ProjectsModule } from './projects/projects.module';
import { UsersModule } from './users/users.module';
import { UserRightsModule } from './rights/user-rights.module';

@Module({
    imports: [SharedModule, ProjectsModule, UsersModule, UserRightsModule],
    controllers: [],
})
export class AppModule {}
