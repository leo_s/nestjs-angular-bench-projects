import { Sequelize } from 'sequelize-typescript';
import { ConfigService } from '../shared/config/config.service';
import { Project } from '../projects/project.entity';
import { User } from '../users/user.entity';
import {UserRight} from "../rights/user-right.entity";


export const databaseProviders = [
    {
        provide: 'SEQUELIZE',
        useFactory: async (configService: ConfigService) => {
            const sequelize = new Sequelize(configService.sequelizeOrmConfig);
            sequelize.addModels([User, Project, UserRight]);
            await sequelize.sync();
            return sequelize;
        },
        inject: [ConfigService],
    },
];
