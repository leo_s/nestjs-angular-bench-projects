import { ApiProperty } from '@nestjs/swagger';
import { Length, IsString } from 'class-validator';

export class CreateProjectDto {
    @ApiProperty()
    @IsString()
    @Length(3, 60)
    readonly title: string;

    @ApiProperty()
    @IsString()
    readonly description: string;
}
