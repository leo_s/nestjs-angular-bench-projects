import { ApiProperty } from '@nestjs/swagger';
import { Project } from '../project.entity';
import { UserRight } from "../../rights/user-right.entity";
import { ERight } from "../../shared/enum/right";

interface testProject {
    id: number,
    title: string,
    description: string,
    userRight: UserRight,
    right: ERight,
}

export class ProjectDto {
    @ApiProperty()
    readonly id: number;

    @ApiProperty()
    readonly title: string;

    @ApiProperty()
    readonly description: string;

    @ApiProperty()
    readonly right: string;

    constructor(project: Project) {
        this.id = project.id;
        this.title = project.title;
        this.description = project.description;
        this.right = project.userRight ? project.userRight[0].right : '';
    }
}
