import { ApiProperty } from '@nestjs/swagger';
import { Length, IsString, IsOptional } from 'class-validator';

export class UpdateProjectDto {
    @IsOptional()
    @ApiProperty()
    @IsString()
    @Length(3, 60)
    readonly title: string;

    @IsOptional()
    @ApiProperty()
    @IsString()
    readonly description: string;
}
