import {
    Table,
    PrimaryKey,
    AutoIncrement,
    Column,
    DataType,
    Model,
    Length, HasMany,
} from 'sequelize-typescript';
import { UserRight } from "../rights/user-right.entity";

@Table({
    tableName: 'project',
})
export class Project extends Model<Project> {
    @PrimaryKey
    @AutoIncrement
    @Column(DataType.BIGINT)
    id: number;

    @Length({
        min: 3,
        max: 60,
        msg: `The length of post title can't be shorter than 3 and longer than 60 `,
    })
    @Column
    title: string;

    @Column
    description: string;

    @HasMany(() => UserRight)
    userRight: UserRight;
}
