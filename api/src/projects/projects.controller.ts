import {
    Controller,
    Req,
    Body,
    Post, UseGuards, Get, Delete, Param, ParseIntPipe, Put,
} from '@nestjs/common';
import {
    ApiBearerAuth,
    ApiCreatedResponse, ApiOkResponse, ApiParam,
    ApiTags,
} from '@nestjs/swagger';
import { CreateProjectDto } from './dto/create-project.dto';
import { ProjectsService } from './projects.service';
import { Project as ProjectEntity } from './project.entity';
import {AuthGuard} from "@nestjs/passport";
import {ProjectDto} from "./dto/project.dto";
import {UpdateProjectDto} from "./dto/update-project.dto";

@Controller('projects')
@ApiTags('projects')
export class ProjectsController {
    constructor(private readonly projectsService: ProjectsService) {}

    @Post()
    @ApiCreatedResponse({ type: ProjectEntity })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    create(
        @Body() createProjectDto: CreateProjectDto,
        @Req() request,
    ): Promise<ProjectDto> {
        return this.projectsService.create(request.user.id, createProjectDto);
    }

    @Get()
    @ApiOkResponse({ type: [ProjectDto] })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    findAll(
        @Req() request,
    ): Promise<ProjectDto[]> {
        return this.projectsService.findAll(request.user.id);
    }

    @Get(':id')
    @ApiOkResponse({ type: ProjectDto })
    @ApiParam({ name: 'id', required: true })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    findOne(
        @Req() request,
        @Param('id',
        new ParseIntPipe()) id: number
    ): Promise<ProjectDto> {
        return this.projectsService.findOne(request.user.id, id);
    }

    @Put(':id')
    @ApiOkResponse({ type: ProjectEntity })
    @ApiParam({ name: 'id', required: true })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    update(
        @Param('id', new ParseIntPipe()) id: number,
        @Req() request,
        @Body() updateProjectDto: UpdateProjectDto,
    ): Promise<ProjectEntity> {
        return this.projectsService.update(id, updateProjectDto);
    }

    @Delete(':id')
    @ApiOkResponse({ type: ProjectEntity })
    @ApiParam({ name: 'id', required: true })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    delete(
        @Param('id', new ParseIntPipe()) id: number,
        @Req() request,
    ): Promise<ProjectDto> {
        return this.projectsService.delete(request.user.id, id);
    }
}
