import { Project } from './project.entity';
import { UserRight } from "../rights/user-right.entity";

export const projectsProviders = [
    { provide: 'ProjectsRepository', useValue: Project },
    { provide: 'UserRightRepository', useValue: UserRight }
];

