import {HttpException, HttpStatus, Inject, Injectable} from '@nestjs/common';
import { CreateProjectDto } from './dto/create-project.dto';
import { Project } from './project.entity';
import { ProjectDto } from './dto/project.dto';
import {UpdateProjectDto} from "./dto/update-project.dto";
import { UserRight } from "../rights/user-right.entity";
import { ERight } from "../shared/enum/right";
import sequelize, { Sequelize } from "sequelize";

@Injectable()
export class ProjectsService {
    constructor(
        @Inject('ProjectsRepository')
        private readonly projectRepository: typeof Project,
        @Inject('UserRightRepository')
        private readonly userRightRepository: typeof UserRight,
    ) {}

    Op = sequelize.Op;

    async findAll(userId: number) {

        const projects = await this.projectRepository.findAll<Project>(
            {
                include: [{
                    model: UserRight,
                    where: {
                        userId: { [this.Op.eq]: userId },
                        right: { [this.Op.ne]: ERight.BAN }
                    },
                }],
            }
        );
        return projects.map(project => new ProjectDto(project));
    }

    async create(userId: number, createProjectDto: CreateProjectDto) {
        const project = new Project();
        project.title = createProjectDto.title;
        project.description = createProjectDto.description;
        const newProject = await project.save();
        const newProjectId = newProject.getDataValue("id");
        const userRight = new UserRight();
        userRight.right = ERight.ADMIN;
        userRight.projectId = newProjectId;
        userRight.userId = userId;
        await userRight.save();
        return new ProjectDto(newProject);
    }

    async findOne(userId: number, projectId: number) {
        const project = await this.projectRepository.findOne<Project>(
            {
                where: { id: projectId },
                include: [{
                    model: UserRight,
                    as: 'userRight',
                    where: {
                        userId: userId,
                        projectId: projectId,
                        right: ERight.ADMIN,
                    },
                    attributes: {
                        exclude: ['createdAt', 'updatedAt', 'userId', 'id', 'projectId']
                    },
                }],
            }
        );
        if (!project) {
            throw new HttpException('No project found', HttpStatus.NOT_FOUND);
        }
        return new ProjectDto(project);
    }

    async update(id: number, updateProjectDto: UpdateProjectDto) {
        const project = await this.projectRepository.findByPk<Project>(id);
        project.title = updateProjectDto.title || project.title;
        project.description = updateProjectDto.description || project.description;
        return project.save();
    }

    async delete(userId: number, projectId: number) {
        const userRight = await this.userRightRepository.findOne<UserRight>({
            where: {
                projectId: projectId,
                userId: userId,
                right: ERight.ADMIN
            }
        });
        if(!userRight) {
            throw new HttpException('No project found for delete or you have not permission', HttpStatus.NOT_FOUND);
        }
        const userRights = await this.userRightRepository.findAll<UserRight>({
            where: {
                projectId: projectId,
            }
        });
        userRights.map(async userRight => await userRight.destroy());

        const project = await this.projectRepository.findByPk<Project>(projectId);
        await project.destroy();
        return new ProjectDto(project);
    }

}
