import { ApiProperty } from '@nestjs/swagger';
import {IsEnum} from 'class-validator';
import {ERight} from "../../shared/enum/right";

export class CreateRightDto {
    @ApiProperty()
    @IsEnum(ERight)
    readonly right: ERight;
}
