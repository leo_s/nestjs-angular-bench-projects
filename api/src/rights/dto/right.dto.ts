import { ApiProperty } from '@nestjs/swagger';
import {UserRight} from "../user-right.entity";
import { ERight } from "../../shared/enum/right";

export class RightDto {
    @ApiProperty()
    id: number;

    @ApiProperty()
    readonly userId: number;

    @ApiProperty()
    readonly projectId: number;

    @ApiProperty()
    readonly right: ERight;

    constructor(right: UserRight) {
        this.right = right.right;
        this.userId = right.userId;
        this.projectId = right.projectId;
    }
}
