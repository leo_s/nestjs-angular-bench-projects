import { ApiProperty } from '@nestjs/swagger';
import { IClientUserRight } from "../../shared/enum/right";

export class UpdateRightDto {
    @ApiProperty()
    readonly projectRights?: IClientUserRight[];
}
