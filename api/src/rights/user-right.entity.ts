import {
    AutoIncrement, BelongsTo,
    Column,
    CreatedAt,
    DataType,
    DeletedAt, ForeignKey,
    HasMany,
    Model,
    PrimaryKey,
    Table,
    UpdatedAt,
} from 'sequelize-typescript';
import {ERight} from "../shared/enum/right";
import {User} from "../users/user.entity";
import {Project} from "../projects/project.entity";

@Table({
    tableName: 'user_right',
})
export class UserRight extends Model<UserRight> {
    @PrimaryKey
    @AutoIncrement
    @Column(DataType.BIGINT)
    id: number;

    @ForeignKey(() => User)
    @Column({
        type: DataType.BIGINT,
        field: 'user_id',
    })
    userId: number;

    @ForeignKey(() => Project)
    @Column({
        type: DataType.BIGINT,
        field: 'project_id',
    })
    projectId: number;

    @Column({ type: DataType.ENUM(ERight.ADMIN, ERight.WRITE, ERight.READ, ERight.BAN) })
    right: ERight;

    @CreatedAt
    @Column({ field: 'created_at' })
    createdAt: Date;

    @UpdatedAt
    @Column({ field: 'updated_at' })
    updatedAt: Date;

    @BelongsTo(() => Project)
    project: Project;

    @BelongsTo(() => User)
    user: User;
}
