import { Body, Controller, Get, Param, ParseIntPipe, Post, Put, Req, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiCreatedResponse, ApiOkResponse, ApiParam } from "@nestjs/swagger";
import {AuthGuard} from "@nestjs/passport";
import {CreateRightDto} from "./dto/create-right.dto";

import {UserRightsService} from "./user-rights.service";
import { UserRight, UserRight as UserRightEntity } from "./user-right.entity";
import { ProjectDto } from "../projects/dto/project.dto";
import { RightDto } from "./dto/right.dto";
import { UpdateRightDto } from "./dto/update-right.dto";

@Controller('rights')
export class UserRightsController {
    constructor(private readonly rightService: UserRightsService) {}

    @Post()
    @ApiCreatedResponse({ type: UserRightEntity })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    create(
        @Body() createRightDto: CreateRightDto,
        @Req() request,
    ): Promise<UserRightEntity> {
        return this.rightService.create(createRightDto);
    }

    @Get(':id')
    @ApiOkResponse({ type: ProjectDto })
    @ApiParam({ name: 'id', required: true })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    findProjectRights(
        @Param('id', new ParseIntPipe()) id: number,
        @Req() request,
    ): Promise<RightDto[]> {
        return this.rightService.findProjectRights(request.user.id, id);
    }

    @Put(':id')
    @ApiOkResponse({ type: UserRight })
    @ApiParam({ name: 'id', required: true })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    update(
        @Param('id', new ParseIntPipe()) id: number,
        @Req() request,
        @Body() updateRightDto: UpdateRightDto,
    ): Promise<RightDto[]> {
        return this.rightService.update(id, updateRightDto);
    }

}
