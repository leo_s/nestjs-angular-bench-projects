import { Module } from '@nestjs/common';
import { UserRightsController } from './user-rights.controller';
import { UserRightsService } from './user-rights.service';
import {rightsProviders} from "./user-rights.providers";
import {DatabaseModule} from "../database/database.module";

@Module({
  imports: [DatabaseModule],
  controllers: [UserRightsController],
  providers: [UserRightsService, ...rightsProviders]
})
export class UserRightsModule {}
