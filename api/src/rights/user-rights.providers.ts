import { UserRight } from './user-right.entity';

export const rightsProviders = [{ provide: 'RightsRepository', useValue: UserRight }];
