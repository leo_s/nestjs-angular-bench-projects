import { HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common';
import {UserRight} from "./user-right.entity";
import {CreateRightDto} from "./dto/create-right.dto";
import { RightDto } from "./dto/right.dto";
import { User } from "../users/user.entity";
import { UpdateRightDto } from "./dto/update-right.dto";

@Injectable()
export class UserRightsService {
    constructor(
        @Inject('RightsRepository')
        private readonly rightRepository: typeof UserRight
    ) {}

    async create(createRightDto: CreateRightDto) {
        const userRight = new UserRight();
        userRight.right = createRightDto.right;
        return userRight.save();
    }

    async findProjectRights(userId: number, projectId: number) {
        const userRight = await this.rightRepository.findAll<UserRight>(
            {
                where: { projectId },
                attributes: {
                    exclude: ['createdAt', 'updatedAt', 'userId']
                },
                include: [{ model: User, attributes: {
                    exclude: ['password', 'email', 'createdAt', 'updatedAt']
                } }],
            });
        if (!userRight) {
            throw new HttpException('No right found', HttpStatus.NOT_FOUND);
        }
        return userRight.map(right => new RightDto(right));
    }

    async update(projectId: number, updateRightDto: UpdateRightDto) {
        for(const updateRight of updateRightDto.projectRights){
            const oldRight = await this.rightRepository.findOne<UserRight>({
                where: {projectId: projectId, userId: updateRight.id},
            });
            if (!oldRight) {
                const newRight = new UserRight();
                newRight.right = updateRight.right;
                newRight.projectId = projectId;
                newRight.userId = updateRight.id;
                await newRight.save();
            } else {
                oldRight.right = updateRight.right;
                await oldRight.save();
            }
        }
        const newRights = await this.rightRepository.findAll<UserRight>({
            where: { projectId: projectId },
        });
        return newRights.map(right => new RightDto(right));
    }
}
