export enum ERight {
    ADMIN = 'ADMIN',
    WRITE = 'WRITE',
    READ = 'READ',
    BAN = 'BAN'
}

export interface IClientUserRight {
    id: number;
    firstName: string;
    lastName: string;
    right: ERight;
}
