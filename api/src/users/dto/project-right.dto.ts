import { User } from '../user.entity';
import { ApiProperty } from '@nestjs/swagger';
import { ERight } from "../../shared/enum/right";
import { UserRight } from "../../rights/user-right.entity";

export class ProjectRightDto {
    @ApiProperty()
    id: number;

    @ApiProperty()
    readonly firstName: string;

    @ApiProperty()
    readonly lastName: string;

    @ApiProperty()
    readonly userRight: UserRight;

    constructor(user: User) {
        this.id = user.id;
        this.firstName = user.firstName;
        this.lastName = user.lastName;
        this.userRight = user.userRight;
    }
}
