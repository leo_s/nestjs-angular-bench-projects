import {
    Table,
    Column,
    Model,
    Unique,
    IsEmail,
    DataType, PrimaryKey, AutoIncrement, CreatedAt, UpdatedAt, HasMany,
} from 'sequelize-typescript';
import {UserRight} from "../rights/user-right.entity";

@Table({
    tableName: 'user',
})
export class User extends Model<User> {
    @PrimaryKey
    @AutoIncrement
    @Column(DataType.BIGINT)
    id: number;

    @Unique
    @IsEmail
    @Column
    email: string;

    @Column
    password: string;

    @Column({ field: 'first_name' })
    firstName: string;

    @Column({ field: 'last_name' })
    lastName: string;

    @CreatedAt
    @Column({ field: 'created_at' })
    createdAt: Date;

    @UpdatedAt
    @Column({ field: 'updated_at' })
    updatedAt: Date;

    @HasMany(() => UserRight)
    userRight: UserRight;
}
