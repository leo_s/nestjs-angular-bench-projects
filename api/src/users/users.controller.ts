import { UserLoginRequestDto } from './dto/user-login-request.dto';
import {
    Controller,
    Get,
    Post,
    Body,
    HttpCode,
    Delete,
    Req,
    UseGuards,
    Put, Param, ParseIntPipe,
} from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UsersService } from './users.service';
import { ApiTags, ApiOkResponse, ApiBearerAuth, ApiParam } from '@nestjs/swagger';
import { UserLoginResponseDto } from './dto/user-login-response.dto';
import { ProjectDto } from "../projects/dto/project.dto";
import { AuthGuard } from "@nestjs/passport";
import { UserDto } from "./dto/user.dto";
import { ProjectRightDto } from "./dto/project-right.dto";

@Controller('users')
@ApiTags('users')
export class UsersController {
    constructor(private readonly usersService: UsersService) {}

    @Post('register')
    @ApiOkResponse({ type: UserLoginResponseDto })
    register(
        @Body() createUserDto: CreateUserDto,
    ): Promise<UserLoginResponseDto> {
        return this.usersService.create(createUserDto);
    }

    @Post('login')
    @HttpCode(200)
    @ApiOkResponse({ type: UserLoginResponseDto })
    login(
        @Body() userLoginRequestDto: UserLoginRequestDto,
    ): Promise<UserLoginResponseDto> {
        return this.usersService.login(userLoginRequestDto);
    }

    @Get(':id')
    @ApiOkResponse({ type: [ProjectDto] })
    @ApiParam({ name: 'id', required: true })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    findAll(
        @Param('id', new ParseIntPipe()) id: number,
        @Req() request,
    ): Promise<ProjectRightDto[]> {
        return this.usersService.findAll(request.user.id, id);
    }

}
