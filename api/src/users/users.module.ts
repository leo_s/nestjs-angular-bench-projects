import { Module } from '@nestjs/common';
import { UsersController } from './users.controller';
import { usersProviders } from './users.providers';
import { DatabaseModule } from '../database/database.module';
import { UsersService } from './users.service';
import {JwtModule} from "@nestjs/jwt";
import {JwtStrategy} from "./auth/jwt-strategy";
import {config} from "../../config/config.development";

@Module({
    imports: [
        DatabaseModule,
        JwtModule.register({
            secret: config.jwtPrivateKey,
            signOptions: config.signOptions,
        })
    ],

    controllers: [UsersController],
    providers: [UsersService, ...usersProviders, JwtStrategy],
    exports: [UsersService],
})
export class UsersModule {}
