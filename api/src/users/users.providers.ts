import { User } from './user.entity';
import { UserRight } from "../rights/user-right.entity";

export const usersProviders = [
    { provide: 'UsersRepository', useValue: User },
    { provide: 'UserRightRepository', useValue: UserRight }
    ];
