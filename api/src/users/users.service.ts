import { Injectable, Inject, HttpException, HttpStatus } from '@nestjs/common';
import { User } from './user.entity';
import { genSalt, hash, compare } from 'bcrypt';
import { CreateUserDto } from './dto/create-user.dto';
import { UserLoginResponseDto } from './dto/user-login-response.dto';
import { JwtPayload } from './auth/jwt-payload.model';
import { ConfigService } from '../shared/config/config.service';
import {UserLoginRequestDto} from "./dto/user-login-request.dto";
import {JwtService} from "@nestjs/jwt";
import { ProjectRightDto } from "./dto/project-right.dto";
import { UserRight } from "../rights/user-right.entity";
import { ERight } from "../shared/enum/right";

@Injectable()
export class UsersService {
    private readonly jwtPrivateKey: string;

    constructor(
        @Inject('UsersRepository')
        private readonly usersRepository: typeof User,
        @Inject('UserRightRepository')
        private readonly userRightRepository: typeof UserRight,
        private readonly configService: ConfigService,
        private readonly jwtService: JwtService,
    ) {
        this.jwtPrivateKey = this.configService.jwtConfig.privateKey;
    }

    async findAll(userId: number, projectId: number) {
        const userRight = await this.userRightRepository.findOne<UserRight>({
            where: {
                projectId: projectId,
                userId: userId,
                right: ERight.ADMIN
            }
        });
        if(!userRight) {
            throw new HttpException(
                'You have not permission to edit these rights.',
                HttpStatus.BAD_REQUEST,
            );
        }
        const users = await this.usersRepository.findAll<User>(
            {
                attributes: {
                    exclude: ['password', 'email', 'createdAt', 'updatedAt']
                },
                include: [
                    {   model: UserRight,
                        where: { projectId: projectId },
                        attributes: {
                            exclude: ['createdAt', 'updatedAt', 'userId', 'id', 'projectId']
                        },
                        required:false
                    }],
            }
        );
        return users.map(user => new ProjectRightDto(user));
    }

    async getUserByEmail(email: string) {
        return this.usersRepository.findOne<User>({
            where: {email},
        });
    }

    async create(createUserDto: CreateUserDto) {
        try {
            const user = new User();
            user.email = createUserDto.email.trim().toLowerCase();
            user.firstName = createUserDto.firstName;
            user.lastName = createUserDto.lastName;

            const salt = await genSalt(10);
            user.password = await hash(createUserDto.password, salt);

            const userData = await user.save();

            const token = await this.signToken(userData);
            return new UserLoginResponseDto(userData, token);
        } catch (err) {
            if (err.original.constraint === 'user_email_key') {
                throw new HttpException(
                    `User with email '${err.errors[0].value}' already exists`,
                    HttpStatus.CONFLICT,
                );
            }
            throw new HttpException(err, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    async login(userLoginRequestDto: UserLoginRequestDto) {
        const email = userLoginRequestDto.email;
        const password = userLoginRequestDto.password;

        const user = await this.getUserByEmail(email);
        if (!user) {
            throw new HttpException(
                'Invalid email or password.',
                HttpStatus.BAD_REQUEST,
            );
        }

        const isMatch = await compare(password, user.password);
        if (!isMatch) {
            throw new HttpException(
                'Invalid email or password.',
                HttpStatus.BAD_REQUEST,
            );
        }
        const token = await this.signToken(user);
        return new UserLoginResponseDto(user, token);
    }

    async signToken(user: User) {
        const payload: JwtPayload = {
            email: user.email,
        };

        return this.jwtService.signAsync(payload);
    }
}
