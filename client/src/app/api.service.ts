import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { environment } from '../environments/environment';
import { catchError } from "rxjs/operators";
import { Router } from "@angular/router";
import { TokenService } from "./shared/token.service";

export interface Parameter {
  [key: string]: any;
}

interface Header {
  [key: string]: any;
}

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private apiUrl: string = environment.apiUrl;

  constructor(private http: HttpClient,
              private router: Router,
              private tokenService: TokenService) {}

  post(path: string, params: Parameter = {}): Observable<any> {
    return this.request('post', path, params );
  }

  put(path: string, params: Parameter = {}): Observable<any> {
    return this.request('put', path, params);
  }

  get(path: string, headers: Header = {}): Observable<any> {
    return this.request('get', path, headers);
  }

  delete(path: string, headers: Header = {}): Observable<any[]> {
    return this.request('delete', path, headers);
  }

  request(method: string, path: string, params: Parameter = {}): Observable<any> {
        const requestPath = `${this.apiUrl}${path}`;
    const hasBody = ['post', 'patch', 'put', 'delete'].includes(method);

    return this.http.request(method, requestPath, {
          body: hasBody ? params : {},
          params: !hasBody ? params : {},
        }).pipe(catchError(val => {
          if(val.message.toLowerCase().includes('unauthorized')) {
            this.tokenService.signOut();
            this.router.navigate(["login"]).then();
          }
          return of([]);
        }))
      };
}
