import { Component, OnDestroy, OnInit } from '@angular/core';
import { TokenService } from "./shared/token.service";
import { Subscription } from "rxjs";
import { delay } from "rxjs/operators";
import { EMAIL_KEY, NAME_KEY } from "./shared/types"

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  isLogged: boolean;
  userName: string;
  userEmail: string
  subscription: Subscription = new Subscription();

  constructor(private tokenService: TokenService) { }

  ngOnInit(): void {
    this.isLogged = !!this.tokenService.getToken();
    this.subscription.add(this.tokenService.isLogged$
      .pipe(
        delay(0),
      )
      .subscribe( isLogged => {
        this.isLogged = isLogged;
        this.userEmail = sessionStorage.getItem(EMAIL_KEY);
        this.userName = sessionStorage.getItem(NAME_KEY);
      })
    )
  }

  logout(): void {
    this.tokenService.signOut();
    window.location.reload();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
