import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProjectComponent } from './project/project.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import { RegisterComponent } from './register/register.component';
import { authInterceptorProviders } from "./shared/auth.interceptor";
import { LoginComponent } from './login/login.component';
import { ProjectsComponent } from './projects/projects.component';
import { FaIconLibrary, FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { faCoffee } from "@fortawesome/free-solid-svg-icons";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DeleteComponent } from './projects/controls/delete/delete.component';
import { EditComponent } from './projects/controls/edit/edit.component';
import { EditProjectComponent } from './edit-project/edit-project.component';
import { ProjectMenuComponent } from './project/project-menu/project-menu.component';
import { ProjectRightsComponent } from './project/project-rights/project-rights.component';
import { RadioButtonComponent } from './shared/controls/radio-button/radio-button.component';

@NgModule({
  declarations: [
    AppComponent,
    ProjectComponent,
    RegisterComponent,
    LoginComponent,
    ProjectsComponent,
    DeleteComponent,
    EditComponent,
    EditProjectComponent,
    ProjectMenuComponent,
    ProjectRightsComponent,
    RadioButtonComponent
  ],
    imports: [
      BrowserModule,
      AppRoutingModule,
      ReactiveFormsModule,
      HttpClientModule,
      FormsModule,
      FontAwesomeModule,
      NgbModule
    ],
  providers: [authInterceptorProviders],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(library: FaIconLibrary) {
    library.addIcons(faCoffee);
  }
}
