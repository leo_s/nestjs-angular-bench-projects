import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { ProjectService } from "../project/project.service";
import { switchMap } from "rxjs/operators";
import { IProject } from "../project/project.component";
import { IApiProject } from "../shared/types";

@Component({
  selector: 'app-edit-project',
  templateUrl: './edit-project.component.html',
  styleUrls: ['./edit-project.component.scss']
})
export class EditProjectComponent implements OnInit {
  editProject: IProject
  constructor(private route: ActivatedRoute, private projectService: ProjectService) { }

  ngOnInit(): void {
    this.route.queryParams
      .pipe(
        switchMap( project => {
          return this.projectService.getProject(project.id);
        })
      )
      .subscribe((project: IApiProject) => {
        this.editProject = project;
        }
      );
  }
}
