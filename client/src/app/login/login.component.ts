import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { Subscription } from "rxjs";
import { UserService } from "../shared/user.service";
import { TokenService } from '../shared/token.service';
import { Router } from '@angular/router';
import { faExclamationTriangle } from "@fortawesome/free-solid-svg-icons";
import { IRegisterUser, IUserToken } from "../register/register.component";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  faExclamationTriangle = faExclamationTriangle;
  emailControl: FormControl = new FormControl(
    null,
    [Validators.required, Validators.email, Validators.pattern('^\\S*[@]\\S*[.]\\S*$')]
  );
  passwordControl: FormControl = new FormControl(
    null,
    [Validators.compose([Validators.minLength(6), Validators.max(Infinity)])]
  );
  loginForm: FormGroup;
  subscription: Subscription = new Subscription();
  isLogged = true;
  notValidEmailMessage = 'E-mail must be valid.';
  passwordErrorMessage = 'Password must be longer than or equal to 6 characters.';
  showEmailErrorIcon = false;
  showPasswordErrorIcon = false;

  constructor(private fb: FormBuilder,
              private loginService: UserService,
              private authService: TokenService,
              private router: Router) { }

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      email: this.emailControl,
      password: this.passwordControl,
    });

    this.subscription.add(this.emailControl.statusChanges.subscribe(() => {
      this.showEmailErrorIcon = !!this.emailControl.errors;
    }));
    this.subscription.add(this.passwordControl.statusChanges.subscribe(() => {
      this.showPasswordErrorIcon = !!this.passwordControl.errors;
    }));
  }

  login(): void {
    const email = this.loginForm.get('email').value;
    const password = this.loginForm.get('password').value;
    this.loginService.login({email, password})
      .subscribe( (res: IRegisterUser & IUserToken ) => {
          this.isLogged = true;
          this.authService.setToken(res.token);
          this.authService.setPersonalData(email, (res.firstName + ' ' + res.lastName));
          this.router.navigate(["projects"]).then();
        },
        () => {
        this.isLogged = false;
          this.router.navigate(["login"]).then();
        })
  }

  get disableSubmitButton(): boolean {
    return this.showPasswordErrorIcon ||
      this.showEmailErrorIcon ||
      !this.emailControl.value ||
      !this.passwordControl.value;
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
