import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from "../api.service";
import { ILoginUser } from "./login.component";
import { IUserToken } from "../register/register.component";

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  constructor(private apiService: ApiService) {
  }

  login(user: ILoginUser): Observable<ILoginUser & IUserToken> {
    return this.apiService.post('users/login', user);
  }
}
