import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

export enum EProjectMenuItem {
  GENERAL = 'General',
  RIGHTS = 'Rights'
}

class ProjectMenu {
  constructor(public title: EProjectMenuItem,
              public active: boolean,
              public disable: boolean) {}
}

@Component({
  selector: 'app-project-menu',
  templateUrl: './project-menu.component.html',
  styleUrls: ['./project-menu.component.scss']
})
export class ProjectMenuComponent implements OnInit {
  @Output() menuItem: EventEmitter<EProjectMenuItem> = new EventEmitter<EProjectMenuItem>();
  projectMenuOperation: boolean;
  menu: ProjectMenu[] = [
    new ProjectMenu(EProjectMenuItem.GENERAL, true, false),
    new ProjectMenu(EProjectMenuItem.RIGHTS, false, true),
  ];
  @Input()
  set userNotAdmin(banEditRight: boolean) {
    this.menu[1].disable = banEditRight;
  }

  @Input()
  set editOperation(redaction: boolean) {
    this.menu.map((item, i) => i !==0 ? item.disable = !redaction : item);
    this.projectMenuOperation = redaction;
  }
  get editOperation(): boolean {
    return this.projectMenuOperation;
  }

  constructor() { }

  ngOnInit(): void {
  }

  onClick(selectedItem: ProjectMenu): void {
    if(!selectedItem.disable) {
      this.menu.map((item => {
        item.active = item.title === selectedItem.title;
        if (this.editOperation) {
          item.disable = false
        }
      }));
      this.menuItem.emit(selectedItem.title);
    }
  }
}
