import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectRightsComponent } from './project-rights.component';

describe('ProjectRightsComponent', () => {
  let component: ProjectRightsComponent;
  let fixture: ComponentFixture<ProjectRightsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectRightsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectRightsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
