import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { FormBuilder, FormGroup } from "@angular/forms";
import { Subscription } from "rxjs";
import { debounceTime } from "rxjs/operators";
import { EProjectRight } from "../project.component";
import { IApiUsers, IClientUserRight } from "../../shared/types";
import { IProjectItemRight } from "../../shared/controls/radio-button/radio-button.component";
import { UserService } from "../../shared/user.service";
import { ProjectService } from "../project.service";

const FILTER_DEBOUNCE_TIME = 300;

interface IRightForm {
  right: IProjectItemRight,
}

@Component({
  selector: 'app-project-rights',
  templateUrl: './project-rights.component.html',
  styleUrls: ['./project-rights.component.scss']
})
export class ProjectRightsComponent implements OnInit, OnDestroy  {
  @Input() projectId: number;
  users: IClientUserRight[] = [];
  projectRightForm: FormGroup;
  subscription: Subscription = new Subscription();

  constructor(
              private userService: UserService,
              private router: Router,
              private fb: FormBuilder,
              private projectService: ProjectService) {}

  ngOnInit(): void {
    this.userService.getUsers(this.projectId)
      .subscribe( (usersRight: IApiUsers[]) => {
        usersRight.forEach( (apiRight: IApiUsers) => {
          const {userRight, ...temp} = apiRight;
          this.users.push({...temp,
            right: apiRight.userRight?.length > 0 ? apiRight.userRight[0].right : EProjectRight.BAN})
        });
      }, () => {
      this.router.navigate(["login"]).then();
    });
    this.projectRightForm = this.fb.group({
      right: [],
    });

    this.subscription.add(this.projectRightForm.valueChanges
      .pipe(
        debounceTime(FILTER_DEBOUNCE_TIME),
      )
      .subscribe((value: IRightForm) => {
        this.users.forEach( (user: IClientUserRight) => {
          if (user.id === value.right.id) {
            user.right = value.right.right;
          }
        });
      }));
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  cancel(): void {
    this.router.navigate(["projects"]).then();
  }

  onEditRights(): void {
    this.projectService.editRights(this.projectId, {projectRights: this.users})
      .subscribe(() => {})
  }
}
