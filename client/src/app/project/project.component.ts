import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { Subscription } from "rxjs";
import { ProjectService } from "./project.service";
import { TokenService } from "../shared/token.service";
import { Router } from "@angular/router";
import { EProjectMenuItem } from "./project-menu/project-menu.component";
import { faExclamationTriangle } from "@fortawesome/free-solid-svg-icons";
import { IApiProject } from "../shared/types";

export interface IProject {
  id?: number;
  title: string;
  description: string;
  right?: EProjectRight;
}

export interface IProjectRight {
  userId: number;
  firstName: string;
  lastName: string;
  right?: EProjectRight;
}

export enum EProjectRight {
  ADMIN = 'ADMIN',
  WRITE = 'WRITE',
  READ = 'READ',
  BAN = 'BAN'
}

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.scss']
})
export class ProjectComponent implements OnInit, OnDestroy {
  @Input() editOperation = false;
  EProjectMenuItem: typeof EProjectMenuItem = EProjectMenuItem;
  menuItem: EProjectMenuItem = EProjectMenuItem.GENERAL;
  projectsForm: FormGroup;
  title: FormControl = new FormControl(
    '',
    [Validators.compose([Validators.minLength(3), Validators.max(Infinity)])]
  );
  description: FormControl = new FormControl(
    '',
    [Validators.compose([Validators.minLength(0), Validators.maxLength(255)])]);
  subscription: Subscription = new Subscription();
  editProjectItem: IApiProject;
  showTitleErrorIcon = false;
  showDescriptionErrorIcon = false;
  faExclamationTriangle = faExclamationTriangle;
  titleErrorMessage = 'Title must be longer than or equal to 3 characters.';
  descriptionErrorMessage = 'Description must be no longer than 255 characters.';
  userNotAdmin = true;

  constructor(private fb: FormBuilder,
              private projectService: ProjectService,
              private tokenService: TokenService,
              private router: Router,
              ) {}

  @Input()
  set editProject(editProject: IApiProject) {
    this.userNotAdmin = editProject?.right !== EProjectRight.ADMIN;

    this.title.setValue(editProject?.title ?? '');
    this.description.setValue(editProject?.description ?? '');
    this.editProjectItem = editProject;
  }

  get editProject(): IApiProject {
    return this.editProjectItem;
  }

  ngOnInit(): void {
    this.projectsForm = this.fb.group({
      title: this.title,
      description: this.description,
    });
    this.tokenService.issueToken();

    this.subscription.add(this.title.statusChanges.subscribe(() => {
      this.showTitleErrorIcon = !!this.title.errors;
    }));
    this.subscription.add(this.description.statusChanges.subscribe(() => {
      this.showDescriptionErrorIcon = !!this.description.errors;
    }));

  }

  saveProject(): void {
    const title = this.projectsForm.get('title').value;
    const description = this.projectsForm.get('description').value;
    this.projectService.addProject({title, description})
      .subscribe((response: IApiProject) => {
        this.editOperation = true;
        this.editProject = response
        this.router.navigate(['/projects/edit'],{ queryParams: { id: response.id } }).then();
      })
  }

  onKeydown(event: KeyboardEvent): void {
    if(event.key === 'Enter') {
      this.saveProject()
    }
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  onEditProject(): void {
    this.editProjectItem.title = this.projectsForm.get('title').value;
    this.editProjectItem.description = this.projectsForm.get('description').value;
    this.projectService.editProject(this.editProjectItem)
      .subscribe(() => {
        this.router.navigate(["projects"]).then();
      })
  }

  cancel(): void {
    this.router.navigate(["projects"]).then();
  }

  navigate(menuItem: EProjectMenuItem): void {
    this.menuItem = menuItem;
  }

  get disableSubmitButton(): boolean {
    return this.showTitleErrorIcon ||
      !this.title.value ||
      this.showDescriptionErrorIcon;
  }
}
