import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from "../api.service";
import { IProject } from "./project.component";
import { IApiProjectRights, IUserRightForApi } from "../shared/types";

@Injectable({
  providedIn: 'root'
})
export class ProjectService {
  constructor(private apiService: ApiService) {
  }

  addProject(project: IProject): Observable<IProject> {
    return this.apiService.post('projects', project);
  }

  editProject(project: IProject): Observable<IProject> {
    return this.apiService.put(`projects/${project.id}`, project);
  }

  editRights(projectId: number, rights: IUserRightForApi): Observable<IProject> {
    return this.apiService.put(`rights/${projectId}`, rights);
  }

  getRights(projectId: number): Observable<IApiProjectRights[]>  {
    return this.apiService.get(`rights/${projectId}`);
  }

  getProjects(): Observable<IProject[]>  {
    return this.apiService.get('projects');
  }

  getProject(id: number): Observable<IProject>  {
    return this.apiService.get(`projects/${id}`);
  }

  deleteProjects(id: number): Observable<IProject[]>  {
    return this.apiService.delete(`projects/${id}`);
  }
}
