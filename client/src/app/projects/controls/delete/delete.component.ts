import { Component, EventEmitter, Input, Output } from '@angular/core';
import { faTrashAlt } from "@fortawesome/free-solid-svg-icons";
import { EProjectRight, IProject } from "../../../project/project.component";

@Component({
  selector: 'project-delete-control',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.scss']
})
export class DeleteComponent {
  @Output() deleteProject: EventEmitter<any> = new EventEmitter();
  faTrash = faTrashAlt;
  tooltipMessage: string
  banDelete = false;

  @Input()
  set editProject(project: IProject) {
    if(project.right === EProjectRight.ADMIN) {
      this.tooltipMessage = 'Delete';
      this.banDelete = false;
    } else {
      this.tooltipMessage = 'Is forbidden, as you are not administrator!';
      this.banDelete = true;
    }
  }

  constructor() { }

  onClick(): void {
    if(!this.banDelete) {
      this.deleteProject.emit();
    }
  }
}
