import { Component, Input } from '@angular/core';
import { faEdit } from "@fortawesome/free-solid-svg-icons/faEdit";
import { EProjectRight, IProject } from "../../../project/project.component";

@Component({
  selector: 'project-edit-control',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent{
  faEdit = faEdit;
  tooltipMessage: string
  banEdit = false;
  _editProject: IProject;

  constructor() {}

  @Input()
  set editProject(project: IProject) {
    if(project.right === EProjectRight.ADMIN) {
      this.tooltipMessage = 'Edit';
      this.banEdit = false;
    } else {
      this.tooltipMessage = 'Is forbidden, as you are not administrator!';
      this.banEdit = true;
    }
    this._editProject = project;
  }

  get editProject(): IProject {
      return this._editProject;
  }
}
