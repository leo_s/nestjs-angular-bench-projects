import { Component, OnInit } from '@angular/core';
import { ProjectService } from "../project/project.service";
import { switchMap } from "rxjs/operators";
import { TokenService } from "../shared/token.service";
import { Router } from "@angular/router";
import { IProject } from "../project/project.component";

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent implements OnInit {
  projects: IProject[]

  constructor(private projectService: ProjectService,
              private tokenService: TokenService,
              private router: Router) { }

  ngOnInit(): void {
     this.projectService.getProjects()
      .subscribe( (res: IProject[]) => {
        this.projects = res;
      },
        () => {
          this.router.navigate(["login"]).then();
        });
    this.tokenService.issueToken();
  }

  deleteProject(id: number) {
    this.projectService.deleteProjects(id)
      .pipe(
        switchMap(() => {
          return this.projectService.getProjects()
        })
      )
      .subscribe( (res: IProject[]) => {
        this.projects = res;
      });
  }
}
