import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { Subscription } from "rxjs";
import { UserService } from "../shared/user.service";
import { Router } from "@angular/router";
import { TokenService } from "../shared/token.service";
import { User } from "./user.model";
import { faExclamationTriangle } from "@fortawesome/free-solid-svg-icons";

export interface IRegisterUser {
  firstName: string,
  lastName: string,
  email: string,
  password: string,
}

export interface IUserToken {
  token: string
}

const NOT_UNIQUE_EMAIL = 'not_unique';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit, OnDestroy {
  faExclamationTriangle = faExclamationTriangle;
  firstNameControl: FormControl = new FormControl();
  lastNameControl: FormControl = new FormControl();
  emailControl: FormControl = new FormControl(
    null,
    [Validators.required, Validators.email, Validators.pattern('^\\S*[@]\\S*[.]\\S*$')]
  );
  passwordControl: FormControl = new FormControl(
    null,
    [Validators.compose([Validators.minLength(6), Validators.max(Infinity)])]
  );
  userForm: FormGroup;
  subscription: Subscription = new Subscription();
  user: User = new User;
  showEmailErrorIcon = false;
  showPasswordErrorIcon = false;
  emailErrorMessage = '';
  notValidEmailMessage = 'E-mail must be valid.';
  notUniqueEmailMessage = 'This email address has already been registered.';
  passwordErrorMessage = 'Password must be longer than or equal to 6 characters.';

  notUniqueEmail = '';

  constructor(private fb: FormBuilder,
              private registerService: UserService,
              private authService: TokenService,
              private router: Router) { }

  ngOnInit(): void {
    this.userForm = this.fb.group({
      firstName: this.firstNameControl,
      lastName: this.lastNameControl,
      email: this.emailControl,
      password: this.passwordControl,
    });

    this.subscription.add(this.emailControl.statusChanges.subscribe(() => {
      this.emailErrorMessage = this.notUniqueEmail === this.emailControl.value ?
        this.notUniqueEmailMessage : this.notValidEmailMessage;
      this.showEmailErrorIcon = !!this.emailControl.errors || this.notUniqueEmail === this.emailControl.value;
    }));
    this.subscription.add(this.passwordControl.statusChanges.subscribe(() => {
      this.showPasswordErrorIcon = !!this.passwordControl.errors;
    }));
  }

  saveUser(): void {
    this.notUniqueEmail = '';
    Object.keys(this.userForm.controls)
      .forEach(c => this.user[c] = this.userForm.controls[c].value);
    this.registerService.addUser(this.user)
      .subscribe( (res: IRegisterUser & IUserToken) => {
          this.authService.setToken(res.token);
          this.authService.setPersonalData(this.user.email, (this.user.firstName + ' ' + this.user.lastName));
          this.router.navigate(["projects"]).then();
      },
        err => {
        if(err.error.errors[0].validatorKey.toLowerCase() === NOT_UNIQUE_EMAIL) {
          this.emailErrorMessage = this.notUniqueEmailMessage;
          this.showEmailErrorIcon = true;
          this.notUniqueEmail = this.emailControl.value;
        }
      })
  }

  get disableSubmitButton(): boolean {
    return this.showPasswordErrorIcon ||
      this.showEmailErrorIcon ||
      !this.firstNameControl.value ||
      !this.lastNameControl.value ||
      !this.emailControl.value ||
      !this.passwordControl.value;
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
