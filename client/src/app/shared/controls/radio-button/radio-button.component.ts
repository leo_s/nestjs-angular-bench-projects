import { Component, forwardRef, Input, OnDestroy } from '@angular/core';
import { ControlValueAccessor, FormBuilder, FormGroup, NG_VALUE_ACCESSOR } from "@angular/forms";
import { Subscription } from "rxjs";
import { EProjectRight } from "../../../project/project.component";
import { IClientUserRight } from "../../types";

export interface IProjectItemRight {
  right: EProjectRight;
  id: number;
}

@Component({
  selector: 'app-radio-button',
  templateUrl: './radio-button.component.html',
  styleUrls: ['./radio-button.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => RadioButtonComponent),
      multi: true
    }
  ]
})
export class RadioButtonComponent implements ControlValueAccessor, OnDestroy {
  componentItemNumber: number;
  radioForm: FormGroup;
  subscriptions: Subscription[] = [];
  EProjectRight: typeof EProjectRight = EProjectRight;
  componentUser: IClientUserRight;

  @Input()
  set user(user: IClientUserRight) {
    this.componentUser = user;
    setTimeout( () => {
      this.radioForm.setValue({right: user.right});
    }, 0)
  }

  get user() {
    return this.componentUser;
  }

  get value(): IProjectItemRight {
    return this.radioForm.value;
  }

  set value(value: IProjectItemRight) {
    this.radioForm.setValue(value);
    this.onChange(value);
    this.onTouched();
  }

  constructor(private fb: FormBuilder) {
    this.radioForm = this.fb.group({
      right: []
    });
    this.subscriptions.push(
      this.radioForm.valueChanges.subscribe(value => {
        this.onChange({...value, id: this.user.id});
        this.onTouched();
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  onChange: any = () => {};
  onTouched: any = () => {};

  registerOnChange(fn) {
    this.onChange = fn;
  }

  writeValue(value) {
    if(value) {
      this.value = value;
    }

    if(value === null) {
      this.radioForm.reset();
    }
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }
}
