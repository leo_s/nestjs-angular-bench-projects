import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from "../api.service";
import { IApiProjectRights } from "./types";

@Injectable({
  providedIn: 'root'
})
export class RightService {
  constructor(private apiService: ApiService) {
  }

  getRights(projectId: number): Observable<IApiProjectRights[]>  {
    return this.apiService.get(`rights/${projectId}`);
  }
}
