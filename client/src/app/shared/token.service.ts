import { Injectable } from '@angular/core';
import { Subject } from "rxjs";
import { EMAIL_KEY, NAME_KEY } from "./types";

const TOKEN_KEY = 'auth-token';

@Injectable({
  providedIn: 'root'
})
export class TokenService {
  isLogged$: Subject<boolean> = new Subject<boolean>();
  constructor() { }

  signOut(): void {
    window.sessionStorage.clear();
    this.isLogged$.next(false);
  }

  public setToken(token: string): void {
    window.sessionStorage.removeItem(TOKEN_KEY);
    window.sessionStorage.setItem(TOKEN_KEY, token);
  }

  public setPersonalData(email: string, name: string): void {
    window.sessionStorage.removeItem(EMAIL_KEY);
    window.sessionStorage.setItem(EMAIL_KEY, email);
    window.sessionStorage.removeItem(NAME_KEY);
    window.sessionStorage.setItem(NAME_KEY, name);
  }

  issueToken(): void {
    this.isLogged$.next(!!this.getToken());
  }

  public getToken(): string {
    return sessionStorage.getItem(TOKEN_KEY);
  }
}
