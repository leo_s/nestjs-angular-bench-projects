import { EProjectRight } from "../project/project.component";

export const EMAIL_KEY = 'user-email';
export const NAME_KEY = 'user-name';

export interface IApiUsers {
  id: number;
  firstName: string;
  lastName: string;
  userRight?: IApiUserRight[]
}

export interface IApiUserRight {
  projectId: number;
  right: EProjectRight;
}

export interface IClientUserRight {
  id: number;
  firstName: string;
  lastName: string;
  right: EProjectRight;
}

export interface IApiProjectRights {
  id: number;
  projectId: number;
  right: EProjectRight;
  userId: number;
}

export interface IUserRightForApi {
  projectRights: IClientUserRight[];
}

export interface IApiProject {
  id: number;
  title: string;
  description: string;
  right: EProjectRight;
}
