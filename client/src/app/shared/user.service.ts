import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from "../api.service";
import { IRegisterUser, IUserToken } from "../register/register.component";
import { User } from "../register/user.model";
import { IApiUsers } from "./types";

export interface ILoginUser {
  email: string,
  password: string
}

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(private apiService: ApiService) {
  }

  addUser(user: User): Observable<IRegisterUser & IUserToken> {
    return this.apiService.post('users/register', user);
  }

  login(user: ILoginUser): Observable<ILoginUser & IUserToken> {
    return this.apiService.post('users/login', user);
  }

  getUsers(projectId: number): Observable<IApiUsers[]>  {
    return this.apiService.get(`users/${projectId}`);
  }
}
